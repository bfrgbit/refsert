require_relative "reference"

def hasNum(ref)
	raise ArgumentError if ref.class != Reference
	return false if not ref.meta
	return false if not ref.meta.has_key? "num"
	return false if not ref.meta["num"]
	return true
end

def compRef(refx, refy)
	raise ArgumentError if refx.class != Reference
	raise ArgumentError if refy.class != Reference
	lt = (4 <=> 9)
	rt = (9 <=> 4)
	if not refx.tag and not refy.tag
		if not hasNum(refx) and not hasNum(refy)
			return lt
		else
			return lt if not hasNum(refx)
			return rt if not hasNum(refy)
			return refx <=> refy
		end
		return lt if not hasNum(refx)
		return rt if not hasNum(refy)
		return refx.meta["num"] <=> refy.meta["num"]
	end
	return lt if not refx.tag
	return rt if not refy.tag
	return refx.tag <=> refy.tag
end

def sort(ls) # references
	raise ArgumentError if ls.class != Array
	for item in ls
		raise ArgumentError if item.class != Reference
	end
	return ls.sort {|refx, refy| compRef(refx, refy)}
end

def main
	raise ArgumentError if ARGV.size != 1

	fName = ARGV[0]
	ls = load(fName)
	ls = sort(ls)
	for ref in ls
		p ref
	end
end

if caller.size < 1 # main
	main
end
