class Reference
	def initialize(tag, text, meta = nil)
		raise ArgumentError if tag != nil and tag.class != String
		raise ArgumentError if text.class != String
		raise ArgumentError if meta != nil and meta.class != Hash
		@tag = tag
		@text = text
		@meta = meta
	end

	def Reference.REGEX_TAG; /\[([A-Za-z]+\d+[a-z]*|[A-Za-z]{2,})\]/ end
	def Reference.REGEX_NUM; /\[(\d+)\]/ end

	def tag; @tag end
	def meta; @meta end

	def to_s; "[#{@tag}] #{@text}" end
end

def load(fName)
	raise ArgumentError if fName.class != String
	raise ArgumentError if fName.size < 1
	f = open(fName)
	ls = []
	for ln in f
		item = parseLn(ln.strip)
		ls << item if item
	end
	f.close
	return ls
end

def parseLn(ln)
	raise ArgumentError if ln.class != String
	tag = nil
	num = nil
	if ln =~ Reference.REGEX_TAG
		tag = $1
	end
	if ln =~ Reference.REGEX_NUM
		num = $1.to_i
	end
	return nil if not tag and not num
	ind = ln.rindex(/\]/)+1
	text = formatText(ln[ind..-1])
	meta = {}
	meta["num"] = num if num.class == 9.class
	ref = Reference.new(tag, text, meta)
	return ref
end

def formatText(text)
	raise ArgumentError if text.class != String
	text.strip!
	return text if text.size < 1
	ind = text.index(/[A-za-z\d]/)
	text = text[ind..-1]
	if text =~ /[A-za-z\d]$/
		text += "."
	end
	return text
end

def main
	raise ArgumentError if ARGV.size != 1

	fName = ARGV[0]
	ls = load(fName)
	for ref in ls
		p ref
	end
end

if caller.size < 1 # main
	main
end
