require_relative "reference"

def filter(fName)
	raise ArgumentError if fName.class != String
	raise ArgumentError if fName.size < 1
	f = open(fName)
	ls = []
	for ln in f
		item = parseLn(ln.strip)
		ls << ln.chomp if not item
	end
	f.close
	return ls
end

def main
	raise ArgumentError if ARGV.size != 1

	fName = ARGV[0]
	ls = filter(fName)
	for ln in ls
		puts ln
	end
end

if caller.size < 1 # main
	main
end
